//
//  ViewController.swift
//  TK13Badge
//
//  Created by Marc Felden on 05/11/2019.
//  Copyright © 2019 Marc Felden. All rights reserved.
//

import UIKit
import M13BadgeView

class ViewController: UIViewController {

    @IBOutlet weak var m13Badge: M13BadgeView!
    @IBOutlet weak var tkBadge: TKBadgeView!
    override func viewDidLoad() {
        super.viewDidLoad()
        m13Badge.text = "13"
        tkBadge.text = "14"
    }


}

